//
//  BlockOfLevelCell.swift
//  tapbarContainerTest
//
//  Created by Angry on 12/12/17.
//  Copyright © 2017 Oleh Ionochkin. All rights reserved.
//

import UIKit

class BlockOfLevelCell: UITableViewCell {

    static let reuseIdentifier = String(describing: BlockOfLevelCell.self)
    static let nib = UINib(nibName: String(describing: BlockOfLevelCell.self), bundle: nil)
    
    @IBOutlet weak var titleLabel: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func update(name: String) {
        titleLabel.text = name
    }
}
