//
//  SkillsViewController.swift
//  tapbarContainerTest
//
//  Created by Angry on 12/12/17.
//  Copyright © 2017 Oleh Ionochkin. All rights reserved.
//

import UIKit

class SkillsViewController: UIViewController {

    @IBOutlet weak var contentView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    func displayContentController(contentController: UIViewController) {
        self.addChildViewController(contentController)
        self.contentView.addSubview(contentController.view)
        contentController.view.frame = CGRect(x: 0, y: 0, width: self.contentView.frame.width, height: self.view.frame.height)
        contentController.didMove(toParentViewController: self)
    }

    @IBAction func powerPressed(_ sender: UIButton) {
        displayContentController(contentController: SkillPowerViewController())
    }
}
