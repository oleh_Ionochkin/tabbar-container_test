//
//  SkillPowerViewController.swift
//  tapbarContainerTest
//
//  Created by Angry on 12/12/17.
//  Copyright © 2017 Oleh Ionochkin. All rights reserved.
//

import UIKit

class SkillPowerViewController: UIViewController {
   
    @IBOutlet weak var tableView: UITableView!
    
    var block: [Block] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupTableView()
        generateBlocks()
    }

    
    // MARK: - Private methods
   
    private func generateBlocks() {
        block.append(Block(name: "1"))
        block.append(Block(name: "2"))
        block.append(Block(name: "3"))
        block.append(Block(name: "4"))
        block.append(Block(name: "5"))
        block.append(Block(name: "6"))
        block.append(Block(name: "7"))
        block.append(Block(name: "8"))
        block.append(Block(name: "9"))
        block.append(Block(name: "10"))
        block.append(Block(name: "11"))
        block.append(Block(name: "12"))

    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.keyboardDismissMode = .onDrag
        tableView.register(BlockOfLevelCell.nib, forCellReuseIdentifier: BlockOfLevelCell.reuseIdentifier )
    }
    private func getBlock(for indexPath: IndexPath) -> Block {
        return block[indexPath.row]
    }
}

// MARK: - UITableViewDatasource
extension SkillPowerViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return block.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: BlockOfLevelCell.reuseIdentifier) as? BlockOfLevelCell else {
            fatalError("Error: Cell doesn't exist")
        }
        let block = getBlock(for: indexPath)
        cell.update(name: block.name)
        return cell
    }
    

}
